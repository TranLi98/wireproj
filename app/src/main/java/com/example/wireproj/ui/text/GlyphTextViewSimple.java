package com.example.wireproj.ui.text;

import android.content.Context;
import android.util.AttributeSet;

import androidx.appcompat.widget.AppCompatTextView;

import com.example.wireproj.ui.utils.TypefaceUtils;

public class GlyphTextViewSimple extends AppCompatTextView {
    public GlyphTextViewSimple(Context context) {
        super(context);
    }

    public GlyphTextViewSimple(Context context, AttributeSet attrs) {
        super(context, attrs);
    }

    public GlyphTextViewSimple(Context context, AttributeSet attrs, int defStyleAttr) {
        super(context, attrs, defStyleAttr);
    }

    private void init(){
        setTypeface(TypefaceUtils.getTypeface(TypefaceUtils.getGlyphsTypefaceName()));
        setSoundEffectsEnabled(false);
    }
}
