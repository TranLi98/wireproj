package com.example.wireproj.ui.text;

import android.graphics.Typeface;

public class TypefaceFactory {

    private static TypefaceFactory typefaceFactory;
    private TypefaceLoader typefaceLoader;

    private TypefaceFactory(){
    }
    public void init(TypefaceLoader typefaceLoader){
        this.typefaceLoader = typefaceLoader;
    }

    public static TypefaceFactory getInstance(){
        if (typefaceFactory == null){
            typefaceFactory = new TypefaceFactory();
        }
        return typefaceFactory;
    }

    /**
     * Return a typeFace with a given name
     */
    public Typeface getTypeface(String name){
        if (typefaceLoader == null){
            throw new IllegalStateException("Init not called with a valid Typefaceloadder");
        }
        return typefaceLoader.getTypeface(name);
    }
}
