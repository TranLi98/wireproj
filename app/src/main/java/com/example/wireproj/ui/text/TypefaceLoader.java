package com.example.wireproj.ui.text;

import android.graphics.Typeface;

public interface TypefaceLoader {
    Typeface getTypeface(String name);
}
