package com.example.wireproj.ui.utils;

import android.graphics.Typeface;

import com.example.wireproj.ui.text.TypefaceFactory;

public class TypefaceUtils {
    /**
     * Returns a typeface with a given name
     */

    public static Typeface getTypeface(String name){
        if (name == null || "".equals(name)){
            return null;
        }
        try {
            return TypefaceFactory.getInstance().getTypeface(name);
        } catch (Throwable t){
            return null;
        }
    }
    public static String getGlyphsTypefaceName(){
        return "wire_glyphs.otf";
    }

    public static String getRedactedTypefaceName(){
        return "redacted_script_regular.ttf";
    }
}
